# Partie Front de mon projet final "VeganHunt" 
## Réalisé sur Next.js

Maquette : https://www.figma.com/file/ydmzNK5CTAs9Fx5ufBDLKN/VeganHunt?type=design&node-id=54%3A391&mode=design&t=jmQk558rUpiAY3Yj-1 
Back-end (symfony) : https://gitlab.com/lauragotti/vegan-back 


Fonctionnalités utilisateur : 
- voir les posts visibles
- recherche par ville / filtrer par tag "100% vegan" ou "options"
- se connecter
- s’inscrire
- modifier son compte
- supprimer son compte
- liker un post
- ajouter un post
- proposer une modification 
- ajouter une photo a un post existant
- modifier une publiation