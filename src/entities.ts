export interface Post {
  id?:number,
  name?:string,
  country?:string,
  city?:string,
  address?:string,
  description:string,
  fullVegan?:boolean,
  visible?:boolean,
  website?:string,
  pictures?:Picture[];
  likes?:number[] //typer like ?
}

export interface Picture {
  id?:number,
  src?:string
  idPost?:number
}

export interface User {
  id?:number,
  name:string,
  email:string,
  profilePicture?:string,
  password:string
  //role??
}

