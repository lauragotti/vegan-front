import { Post } from "@/entities";
import Link from "next/link";
import LikeBtn from "./LikeBtn";
import { AuthContext } from "@/auth/auth-context";
import { useContext, useState } from "react";
import jwtDecode from "jwt-decode";
import { updatePost } from "@/post-service";

interface Props {
  post: Post;
}

export default function PostCard({ post }: Props) {
  const max_length = 80;
  const prefix = process.env.NEXT_PUBLIC_SERVER_URL + '/uploads/thumbnails/'
  const { token } = useContext(AuthContext);
  const [isVisible, setIsVisible] = useState(post.visible);


  function isAdmin() {
    if (token) {
      const decoded = jwtDecode<any>(token);
      if (decoded.roles[0] == "ROLE_ADMIN") {
        return true;
      }
      return false
    }
  }

  async function visible(){
    const updatedPost = { ...post, visible: !post.visible };
    try {
      await updatePost(updatedPost);
      setIsVisible(!isVisible);
    } catch (error) {
      console.error("Error updating post visibility:", error);
    }
  }

  return (
    <>
      <div className="card rounded-4" key={post.id}>
        <div className="row g-1">
          <div className="col-4 d-flex align-items-center justify-content-center overflow-hidden rounded-start-4">
            {post.pictures && post.pictures.length > 0 && (
              <img src={prefix + post.pictures[0].src} className="img-card" alt="" />
            )}
          </div>
          <div className="col-8">
            <div className="card-body">
              <h2 className={`h5 card-title text-capitalize ${isVisible? '' : 'text-danger '}`}>{post.name}
              {isAdmin() && 
              <i className="clickable bi bi-eye-fill fs-2 ms-2" onClick={visible}></i>
               }
              </h2>
              <p><i className="bi bi-geo-alt-fill me-1"></i>{post.city}, {post.country}</p>
              <p className="fs-2">
                {post.description && post.description.length > max_length ? `${post.description.slice(0, max_length)}...` : post.description}
              </p>
              {post.fullVegan ? <p className="d-inline vegan-tag fs-1">100% vegan</p> : <p className="d-inline options-tag fs-1">options</p>}
            </div>
            <div className="card-footer d-flex fs-1 justify-content-between">
              <LikeBtn post={post} />
              <p className="d-inline">
                <Link href={"/post/" + post.id} className=" text-success">Voir plus</Link>
              </p>
            </div>
          </div>
        </div>
      </div>

    </>
  )
}