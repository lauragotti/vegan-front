import { AuthContext } from "@/auth/auth-context";
import Link from "next/link";
import { useContext, useEffect, useState } from "react";
import jwtDecode from "jwt-decode";
import router from "next/router";
import { fetchAllPosts } from "@/post-service";
import Select from 'react-select';

export default function Header() {
  const { token } = useContext(AuthContext);
  const [searchKey, setSearchKey] = useState(0); // Key to force re-render of Select

  const handleSearch = (event: any) => {
    event.preventDefault();
    const searchTerm = event.target.elements.search.value;
    const isEmptyOrSpaces = /^\s*$/.test(searchTerm);

    if (!isEmptyOrSpaces) {
      router.push('/search/' + searchTerm);
      setSearchKey(prevKey => prevKey + 1); // Increment the key to force re-render
    }
  };



  const [cities, setCities] = useState<string[]>([]);

  useEffect(() => {
    fetchAllPosts()
      .then(data => {
        const visibleCities = data.filter(item => item.visible).map(item => item.city);
        const uniqueCities = [...new Set(visibleCities)] as string[];
        console.log(uniqueCities);
        setCities(uniqueCities);
      })
      .catch(error => {
        console.error("Error fetching data:", error);
      });
  }, []);

  const options = cities.map(city => ({
    value: city,
    label: city
  }));

  function modalBtn() {
    router.push('/login')
  }

  return (
    <>
      <nav className="navbar navbar-expand-lg bg-body-tertiary text-success">
        <div className="container-fluid">
          <Link className="navbar-brand" href="/"><img className="img-fluid logo-img" src="veganhunt.png"></img></Link>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarToggler" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarToggler">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <Link className="nav-link" aria-current="page" href="/">Accueil</Link>
              </li>
              <li className="nav-item">
                {token ? 
                <Link className="nav-link" href="/add">Ajoutez un lieu</Link> :
                <a className="nav-link clickable"  data-bs-toggle="modal" data-bs-target="#notLoggedAdd">Ajoutez un lieu</a>
              }
              </li>
            </ul>
            <ul className="navbar-nav">
              <li className="nav-item d-flex align-items-center">
                {token ? <Link href="/account" className="nav-item nav-link">Mon compte</Link> : <Link href="/login" className="nav-item nav-link">Connexion</Link>}
              </li>
            </ul>
          </div>
        </div>
      </nav>

      <div className="search-div row d-flex justify-content-center align-items-center">
        <div className="col-9 col-md-4">
          <p className="text-light text-center fs-6">Trouvez où manger vegan</p>
          <div>
            <form className="d-flex" role="search" onSubmit={handleSearch}>
              <Select
                options={options}
                key={searchKey}
                className="form-control bg-white fs-1"
                placeholder="Recherchez par ville..."
                aria-label="Search"
                name="search"
              />
              <button className="btn btn-success search-btn ms-1" type="submit"><i className="bi bi-search text-white"></i></button>
            </form>
          </div>
        </div>
      </div>

      <div className="modal fade" id="notLoggedAdd" aria-labelledby="notLoggedAddLabel" aria-hidden="true">
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-header">
                  <h1 className="modal-title fs-5" id="notLoggedAddLabel">Vous devez être connecté pour ajouter une publication.</h1>
                  <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div className="modal-body">
                  <div className="d-flex justify-content-center ">
                    <button className="btn btn-primary rounded-pill my-3" data-bs-dismiss="modal" aria-label="Close" onClick={modalBtn}>Connexion</button>
                  </div>
                </div>
                <div className="modal-footer justify-content-center">
                  <button type="button" className="btn btn-secondary rounded-pill" data-bs-dismiss="modal">Annuler</button>
                </div>
              </div>
            </div>
          </div>
    </>
  )
}