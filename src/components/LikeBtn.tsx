import { AuthContext } from "@/auth/auth-context";
import { Post } from "@/entities";
import { fetchLikedPosts, toggleLike } from "@/post-service";
import { useRouter } from "next/router";
import { useContext, useEffect, useState } from "react";

interface Props {
  post: Post;
}

export default function LikeBtn({ post }: Props) {
  const { token, setToken } = useContext(AuthContext);
  const [isLiked, setIsLiked] = useState(false);
  const router = useRouter();

  useEffect(() => {
    if (token) {
      fetchLikedPosts()
        .then(likedPosts => {
          setIsLiked(likedPosts.some(likedPost => likedPost.id === post.id));
        })
        .catch(error => {
          console.error("Error fetching liked posts:", error);
        });
    }
  }, [post.id, token]);

  async function like() {
    try {
      await toggleLike(Number(post.id))
      setIsLiked(prevIsLiked => !prevIsLiked);
    } catch (error) {
      console.error("Error toggling like:", error);
    }
  }

  function modalBtn() {
    router.push('/login')
  }


  return (
    <>
      {token ?
      <div>
        <i className={`me-2 clickable d-inline bi bi-heart${isLiked ? '-fill' : ''} text-success fs-4`} onClick={like}></i>
      </div>
        :
        <>
        <div>
          <i className="clickable d-inline bi bi-heart text-success fs-4 me-2" data-bs-toggle="modal" data-bs-target="#notLogged"></i>

        </div>


          <div className="modal fade" id="notLogged" aria-labelledby="notLoggedLabel" aria-hidden="true">
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-header">
                  <h1 className="modal-title fs-5" id="notLoggedLabel">Vous devez être connecté pour aimer une publication.</h1>
                  <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div className="modal-body">
                  <div className="d-flex justify-content-center ">
                    <button className="btn btn-primary rounded-pill my-3" data-bs-dismiss="modal" aria-label="Close" onClick={modalBtn}>Connexion</button>
                  </div>
                </div>
                <div className="modal-footer justify-content-center">
                  <button type="button" className="btn btn-secondary rounded-pill" data-bs-dismiss="modal">Annuler</button>
                </div>
              </div>
            </div>
          </div>
        </>
      }

    </>
  );
}
