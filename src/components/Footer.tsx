import Link from "next/link";

export default function Footer() {
 

  return (
    <footer className="mt-5 d-flex">
      <div className="row justify-content-center w-100 align-content-center fs-2">
        <div className="col-3 text-center">
          <Link href="/" className="text-light">Accueil</Link>
        </div>
        <div className="col-3 text-center">
          <Link href="/signup" className="text-light">S'inscrire</Link>
        </div>
        <div className="col-3 text-center">
          <Link href="/login" className="text-light d-block">Se connecter</Link>
          
          <Link href="/account" className="text-light d-block">Mon compte</Link>

        </div>
      </div>
    </footer>
  )
}