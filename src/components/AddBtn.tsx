import { AuthContext } from "@/auth/auth-context";
import Link from "next/link";
import router from "next/router";
import { useContext } from "react";

export default function AddBtn() {
  const { token } = useContext(AuthContext);

  function modalBtn() {
    router.push('/login')
  }

  return (
    <div className="d-flex justify-content-center ">
      {token ?
      
      <Link className="btn btn-primary rounded-pill my-3" href="add">Ajoutez un lieu</Link> : 
      <>
      <a className="btn btn-primary rounded-pill my-3"  data-bs-toggle="modal" data-bs-target="#notLogged">Ajoutez un lieu</a>
      <div className="modal fade" id="notLogged" aria-labelledby="notLoggedLabel" aria-hidden="true">
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-header">
                  <h1 className="modal-title fs-5" id="notLoggedLabel">Vous devez être connecté pour ajouter une publication.</h1>
                  <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div className="modal-body">
                  <div className="d-flex justify-content-center ">
                    <button className="btn btn-primary rounded-pill my-3" data-bs-dismiss="modal" aria-label="Close" onClick={modalBtn}>Connexion</button>
                  </div>
                </div>
                <div className="modal-footer justify-content-center">
                  <button type="button" className="btn btn-secondary rounded-pill" data-bs-dismiss="modal">Annuler</button>
                </div>
              </div>
            </div>
          </div>
          </>
        
    }
    </div>
  )
}