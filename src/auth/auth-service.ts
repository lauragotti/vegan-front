import { User } from "@/entities";
import axios from "axios";



export async function login(email:string,password:string) {
    const response = await axios.post<{token:string}>('/api/login', {email, password});
    return response.data.token;
}

export async function fetchUser() {
    const repsonse = await axios.get<User>('/api/protected');
    return repsonse.data;
}

export async function postUser(user:User) {
  const response = await axios.post('/api/user' , user);
  return response.data;
}

export async function updateUser(user:User) {
  const response = await axios.put<User>('/api/protected', user);
  return response.data;
}

export async function updatePassword(oldPassword: string, newPassword: string): Promise<void> {
    const response = await axios.put('/api/protected/password', {
      oldPassword,
      newPassword,
    });

   return response.data
}

export async function deleteUser(user:User) {
  const response = await axios.delete('/api/protected', {data : user});
  return response.data;
}