import axios from "axios";
import Router  from "next/router";
import { parseCookies } from "nookies";

axios.defaults.baseURL = process.env.NEXT_PUBLIC_SERVER_URL;


axios.interceptors.request.use((config) => {
    const { token } = parseCookies();
    if (token) {
        config.headers.setAuthorization('Bearer ' + token);

    }
    return config;
});

axios.interceptors.response.use((response) => response, (error) => {

    if (error.response?.status == 401) {
        Router.push('/login')
    }
    return Promise.reject(error);

});
