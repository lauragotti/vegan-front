import axios from "axios";
import { Picture, Post } from "./entities";


export async function fetchAllPosts() {
    const response = await axios.get<Post[]>('/api/post');
    return response.data;
}


export async function fetchOnePost(id:number){
  const response = await axios.get<Post>('/api/post/'+id);
  return response.data;
}

export async function postOnePost(post: Post){
  const response = await axios.post<Post>('/api/post', post);
  return response.data;
}

export async function postOnePicture(picture: Picture){
  const response = await axios.post<Picture>('api/picture', picture);
  return response.data;
}

export async function fetchLikedPosts(){
  const response = await axios.get<Post[]>('/api/protected/likes');
  return response.data;
}

export async function toggleLike(postId:number){
  axios.post('/api/post/'+postId+'/like')
  return
}

export async function fetchSearch(search:string){
  const response = await axios.get<Post[]>('/api/post/search/'+search);
  return response.data;
}

export async function updatePost(post:Post) {
  const response = await axios.put<Post>('/api/post', post);
  return response.data;
}

export async function deletePicture(idPicture:number){
  const response = await axios.delete('api/picture/'+idPicture);
  return response.data;
}