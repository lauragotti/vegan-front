import Header from '@/components/Header';
import '../styles/custom.scss'
import type { AppProps } from 'next/app'
import { useEffect } from 'react';
import "../auth/axios-config"
import "../../node_modules/bootstrap-icons/font/bootstrap-icons.css";
import { AuthContextProvider } from '@/auth/auth-context';
import Head from 'next/head'

import { Poppins } from 'next/font/google'
import Footer from '@/components/Footer';
const poppins = Poppins({
  subsets: ['latin'],
  weight: ['200', '300', '400', '500']
})

export default function App({ Component, pageProps }: AppProps) {
  useEffect(() => {
    require("bootstrap/dist/js/bootstrap.bundle.min.js");
  }, []);
  return (

    <div className={poppins.className}>
      <Head>
        <title>VeganHunt</title>
        <meta
          name="description"
          content="VeganHunt : plateforme collaborative de recensement de lieux de restauration vegans"
        />
      </Head>
      <AuthContextProvider>
        <Header />
        <main className="container-fluid">
          <div className=" row justify-content-center">
            <div className="col-md-11">
              <Component {...pageProps} />
            </div>
          </div>
        </main>
        <Footer />
      </AuthContextProvider>

    </div>
  )
}
