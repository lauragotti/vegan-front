import { AuthContext } from "@/auth/auth-context";
import { login } from "@/auth/auth-service";
import Link from "next/link";
import { useRouter } from "next/router";
import { useContext, useState } from "react";


export default function LoginForm() {
  const router = useRouter();
  const { token, setToken } = useContext(AuthContext);
  const [error, setError] = useState('');
  const [log, setLog] = useState({
    email: '',
    password: ''
  })

  function handleChange(event: any) {
    setLog({
      ...log,
      [event.target.name]: event.target.value
    })
  }

  async function handleSubmit(event: any) {
    event.preventDefault();
    setError('');
    try {
      setToken(await login(log.email, log.password));
      router.push(('/'));
    } catch (error: any) {
      console.log(error)
      if (error.response?.status == 401) {
        setError('Identifiant/mot de passe invalide');
      } else {
        setError('Server error');
      }
    }
  }

  return (
    <>
      <h1 className="h3 my-3">Se connecter</h1>
      <div className="row justify-content-center">

        {token ?
          <button className="btn btn-danger logout-btn" onClick={() => setToken(null)}>Se déconnecter</button>
          :
          <div className="col-md-6">
            {error && <p className="text-danger">{error}</p>}
            <form onSubmit={handleSubmit}>
              <div className="mb-3">
                <label htmlFor="email" className="form-label">Email*</label>
                <input type="email" className="form-control" id="email" aria-describedby="emailHelp" name="email" onChange={handleChange} required />
              </div>
              <div className="mb-3">
                <label htmlFor="password" className="form-label">Mot de passe*</label>
                <input type="password" className="form-control" id="password" name="password" onChange={handleChange} required />
              </div>
              <div className="text-center">
                <button type="submit" className="btn btn-success text-light rounded-pill">Se connecter</button>
              </div>
            </form>
            <div className="text-center">
              <button className="btn btn-primary mt-2 rounded-pill"><Link href="/signup" className="text-light text-decoration-none ">S'inscrire</Link></button>
            </div>
          </div>
        }
      </div>

    </>
  )
}
