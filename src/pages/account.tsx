import { AuthContext } from "@/auth/auth-context";
import { deleteUser, fetchUser, updatePassword, updateUser } from "@/auth/auth-service";
import PostCard from "@/components/PostCard";
import { Post, User } from "@/entities"
import { fetchLikedPosts } from "@/post-service";
import Link from "next/link";
import { useRouter } from "next/router";
import { useContext, useEffect, useState } from "react"


export default function Account() {
  const [user, setUser] = useState<User>({
    name: '',
    email: '',
    profilePicture: '',
    password: ''
  });

  const [posts, setPosts] = useState<Post[]>()

  const router = useRouter();
  const { token, setToken } = useContext(AuthContext);

  function logout() {
    setToken(null)
    router.push('/')
  }

  useEffect(() => {
    fetchUser().then(data => {
      setUser(data);
    }).catch(error => {
      if (error.response.status == 401) {
        router.push('/login')
      }
    });
    fetchLikedPosts().then(data => {
      setPosts(data);
    })
  }, [])



  function handleChange(event: any) {
    setUser({
      ...user,
      [event.target.name]: event.target.value
    });
  }

  const [updateSuccess, setUpdateSuccess] = useState(false)

  async function update(event: any) {
    event?.preventDefault()
    try {
      const updatedUser = await updateUser(user);
      setUser(updatedUser);
      setUpdateSuccess(true)
    } catch (error:any) {
      setUpdateSuccess(false)

      setError(error.response.data)
    }
  }


  //update pw
  const [oldPassword, setOldPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [error, setError] = useState('');
  const [passwordConfirmation, setPasswordConfirmation] = useState('');
  const [passwordSuccess, setPasswordSuccess] = useState(false)
  const handlePasswordUpdate = async (event: any) => {
    event.preventDefault();
    if (!oldPassword || !newPassword) {
      setError('Remplissez tous les champs');
      return;
    }
    if (newPassword !== passwordConfirmation) {
      setError('Le mot de passe et la confirmation du mot de passe ne correspondent pas.');
      return;
    }
    try {
      await updatePassword(oldPassword, newPassword);
      setError('');
      setPasswordSuccess(true)
      // router.reload();
    } catch (error: any) {
      if (error.response && error.response.status === 400) {
        if (error.response.data.errors) {
          const validationErrors = error.response.data.errors;
          const errorMessages = validationErrors.detail.split('\n');
          setError(errorMessages);
          setPasswordSuccess(false)
        } else {
          setError(error.response.data);
          setPasswordSuccess(false)
        }
      } else {
        setError('Server error');
        setPasswordSuccess(false)
      }
    }
  }

  //delete
  async function deleteAccount(event:any) {
    event.preventDefault()
    try {
      await deleteUser(user);
      setToken(null)
      router.push('/')
    } catch 
       (error : any) {
        setError('Server error')
    }
  }

  return (
    <>
      <h1 className="h3 mt-3">{user?.name}</h1>
      <a onClick={logout} className="fs-2 mb-3 text-body-secondary clickable">Se déconnecter</a>
      <h2 className="h5 mt-3">Mes favoris</h2>
      <div className="row">
        {posts && posts.length > 0 ? (
          posts.map((item) => (
            item.visible && (
              <div className="col-lg-6 p-2" key={item.id}>
                <PostCard post={item} />
              </div>
            )
          ))
        ) : (
          <p>Vous n'avez pas de favoris, aimez des publications pour en ajouter.</p>
        )}
      </div>
      <h2 className="h5 mt-3">Mes informations</h2>
      <div className="row">
        <div className="col-md-6">
          <p >Nom d'utilisateur : {user.name}</p>
          <p>Email : {user.email}</p>
        </div>
        <div className="d-flex justify-content-center">
          <button className="btn btn-primary text-white rounded-pill my-3" data-bs-toggle="modal" data-bs-target="#updateModal"  >Modifier mes informations</button>
        </div>
        <div className="d-flex justify-content-center">
          <button className="btn btn-primary text-white rounded-pill my-3" data-bs-toggle="modal" data-bs-target="#updatePasswordModal"  >Modifier mon mot de passe</button>
        </div>
        <div className="d-flex justify-content-center">
          <a className="text-danger clickable"  data-bs-toggle="modal" data-bs-target="#deleteModal">Supprimer mon compte</a>
        </div>
      </div>


      <div className="modal fade" id="updateModal" aria-labelledby="updateModalLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h1 className="modal-title fs-5" id="updateModalLabel">Modifiez vos informations</h1>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div className="modal-body">
              <form onSubmit={update} className="" id="form">
              {error && <p className="text-danger">{error}</p>}
              {updateSuccess && <p className="text-success">Vos informations ont été modifiées, vous pouvez fermer cette modale.</p>}

                <div className="mb-3">
                  <label htmlFor="name" className="form-label ">Nom d'utilisateur</label>
                  <input type="text" name="name" value={user.name} onChange={handleChange} className="form-control" id="name" />
                </div>
                <div className="mb-3">
                  <label htmlFor="email" className="form-label ">Email</label>
                  <input type="text" name="email" value={user.email} onChange={handleChange} className="form-control" id="email" />
                </div>
                <div className="d-flex justify-content-center">
                  <button type="submit" className="btn btn-success text-white rounded-pill my-3">Modifier mes informations</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

      <div className="modal fade" id="updatePasswordModal" aria-labelledby="updatePasswordModalLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h1 className="modal-title fs-5" id="updatePasswordModalLabel">Modifiez votre mot de passe</h1>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div className="modal-body">
              <form onSubmit={handlePasswordUpdate}>
                {error && <p className="text-danger">{error}</p>}
                {passwordSuccess && <p className="text-success">Le mot de passe a été modifié, vous pouvez fermer cette modale.</p>}
                <div className="mb-3">
                  <label htmlFor="oldPassword" className="form-label">Ancien mot de passe*</label>
                  <input type="password" value={oldPassword} onChange={(e) => setOldPassword(e.target.value)} className="form-control" id="oldPassword" required />
                </div>
                <div className="mb-3">
                  <label htmlFor="newPassword" className="form-label">Nouveau mot de passe* (8 caractères minimum)</label>
                  <input type="password" value={newPassword} onChange={(e) => setNewPassword(e.target.value)} className="form-control" id="newPassword" required />
                </div>
                <div className="mb-3">
                  <label htmlFor="passwordConfirmation" className="form-label">Confirmer le mot de passe*</label>
                  <input
                    type="password"
                    name="passwordConfirmation"
                    value={passwordConfirmation}
                    onChange={(event) => setPasswordConfirmation(event.target.value)}
                    className="form-control"
                    id="passwordConfirmation"
                    required
                  />
                </div>
                <div className="d-flex justify-content-center">
                  <button type="submit" className="btn btn-success text-white rounded-pill my-3" >Modifier</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

      <div className="modal fade" id="deleteModal" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h1 className="modal-title fs-5" id="deleteModalLabel">Supprimez votre compte</h1>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div className="modal-body">
              <p>Etes-vous sûr de vouloir supprimer votre compte ?</p>
              <form onSubmit={deleteAccount} className="" id="delete-form">
                <div className="d-flex justify-content-center">
                  <button type="submit" className="btn btn-danger text-white rounded-pill my-3" data-bs-dismiss="modal" aria-label="Close">Supprimer</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>

  )
}