import PostCard from "@/components/PostCard";
import { Post } from "@/entities";
import { fetchSearch } from "@/post-service";
import { GetServerSideProps } from "next";
import { useMemo, useState } from "react";

interface Props {
  posts: Post[];
  query: string;
}

export default function ResultPage({ posts, query }: Props) {
  const [formValue, setFormValue] = useState('default')
  const defaultPosts = posts;


  const renderedPosts = useMemo(() => {
    if (formValue === "vegan") {
      return [...posts].filter(item => item.fullVegan === true);

    } else if (formValue === "options") {
      return [...posts].filter(item => item.fullVegan === false);
    } else {
      console.log(defaultPosts);
      return defaultPosts;

    }
  }, [formValue, posts]);

  function sortBy(e: any) {
    setFormValue(e.target.value);
  }


  return (
    <>
      <h1 className="h3 my-3">Résultats de recherche pour : {query}</h1>
      <div className="row">
        {posts.length > 0 &&
          <div className="row mb-3">
            <form>
            <p className="fs-3">Filtrer:</p>
            <div className="form-check form-check-inline">
                <input className="form-check-input me-1 clickable" type="radio" name="filter" id="default" value="default" onChange={sortBy} />
                <label className="form-check-label " htmlFor="default">
                  Tout
                </label>
                </div>
                <div className="form-check form-check-inline">
                <input className="form-check-input me-1 clickable" type="radio" name="filter" id="vegan" value="vegan" onChange={sortBy} />
                <label className="form-check-label " htmlFor="vegan">
                  100% vegan
                </label>
              </div>
              <div className="form-check form-check-inline">
                <input className="form-check-input me-1 clickable" type="radio" name="filter" id="options" value="options" onChange={sortBy} />
                <label className="form-check-label" htmlFor="options">
                  Options vegan
                </label>
              </div>
            </form>
          </div>
        }

        {renderedPosts.length > 0 ? (

          renderedPosts.map((item) =>
            item.visible &&
            <div className="col-lg-6 p-2"  key={item.id}>
              <PostCard post={item} />
            </div>
          )
        ) : (
          <p>Pas de résultats</p>
        )}
      </div>
    </>
  );
}

export const getServerSideProps: GetServerSideProps<Props> = async (
  context
) => {
  const { result } = context.query;

  try {
    const posts = await fetchSearch(String(result));
    return {
      props: {
        posts,
        query: String(result),
      },
    };
  } catch (error) {
    console.log("Error fetching search results:", error);
    return {
      notFound: true,
    };
  }
};
