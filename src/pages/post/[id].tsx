import { AuthContext } from "@/auth/auth-context";
import LikeBtn from "@/components/LikeBtn";
import { Picture, Post } from "@/entities";
import { deletePicture, fetchOnePost, postOnePicture, updatePost } from "@/post-service";
import { GetServerSideProps } from "next";
import Link from "next/link";
import { useRouter } from "next/router";
import { MutableRefObject, useContext, useState } from "react";

import { useKeenSlider } from "keen-slider/react"
import "keen-slider/keen-slider.min.css"
import jwtDecode from "jwt-decode";

interface Props {
  post: Post;
}

export default function OnePost({ post }: Props) {
  const [ref] = useKeenSlider<HTMLDivElement>({
    breakpoints: {
      "(min-width: 400px)": {
        slides: { perView: 2, spacing: 5 },
      },
      "(min-width: 1000px)": {
        slides: { perView: 3, spacing: 10 },
      },
    },
    slides: { perView: 1 },
  })

  const { token } = useContext(AuthContext);
  const router = useRouter();
  const prefixThumbnail = process.env.NEXT_PUBLIC_SERVER_URL + '/uploads/thumbnails/'
  const prefix = process.env.NEXT_PUBLIC_SERVER_URL + '/uploads/'
  const [errors, setErrors] = useState('')

  function modalBtn() {
    router.push('/login')
  }

  const [picture, setPicture] = useState<Picture>(
    {
      src: '',
      idPost: post.id
    }
  );


  function handleFile(event: any) {
    const reader = new FileReader();
    reader.onload = () => {

      setPicture({
        ...picture,
        src: reader.result as string
      })
    }

    reader.readAsDataURL(event.target.files[0]);
  }

  async function handleSubmit(event: any) {
    event.preventDefault();
    try {
      await postOnePicture(picture);
      router.reload()
    } catch (error: any) {
      if (error.response.status == 400) {
        setErrors(error.response.data.detail)
      }
    }
  }

  //update
 
  const [updatedPost, setUpdatedPost] = useState(post);

  async function handleUpdateChange(event: any) {
    if (event.target.name == "fullVegan") {
      setUpdatedPost({
        ...post,
        [event.target.name]: event.target.value == "true" ? true : false
      });
    } else {
      setUpdatedPost({
        ...post,
        [event.target.name]: event.target.value
      });
    }
  }

  async function updateSubmit(event: any) {
    event.preventDefault();
    try {
      await updatePost(updatedPost);
      setUpdatedPost(updatedPost)
      // router.reload()
    } catch (error: any) {
      if (error.response.status == 400) {
        setErrors(error.response.data.detail)
      }
    }
  }

  //delete picture
  function isAdmin() {
    if (token) {
      const decoded = jwtDecode<any>(token);
      if (decoded.roles[0] == "ROLE_ADMIN") {
        return true;
      }
      return false
    }
  }

  const [idPicture, setIdPicture] = useState(0)
  const [error, setError] = useState("");

  async function deletePic(event:any) {
    event.preventDefault()
    try {
      await deletePicture(idPicture);
      router.reload()
    } catch 
       (error : any) {
        setError('Server error')
    }
  }

  return (
    <>
      <h1 className="h3 my-3">{updatedPost.name}</h1>
      <p>{updatedPost.address}, {updatedPost.city}, {updatedPost.country}</p>

      <div ref={ref} className="keen-slider mb-3">
        {updatedPost.pictures?.map(item =>
        <>
          <img src={prefix + item.src} key={item.id} className="keen-slider__slide clickable img-fluid" />
        </>
        )}
      </div>

      <p className="my-2">{updatedPost.description}</p>

      {updatedPost.website &&
        <a href={updatedPost.website} className="text-success my-2" target="_blank">{updatedPost.website}</a>
      }

      <div className=" d-flex my-3">
        {updatedPost.fullVegan ? <p className="vegan-tag">100% vegan</p> : <p className="options-tag">options</p>}
      </div>


      <div className="d-flex my-2">
        <LikeBtn post={post} />
      </div>

      <div className="d-flex justify-content-center ">
        <button className="btn btn-success rounded-pill my-3 text-white" data-bs-toggle="modal" data-bs-target="#addPicture" >Ajoutez une photo</button>
      </div>

      <div className="d-flex justify-content-center ">
        <button className="btn btn-primary rounded-pill my-3" data-bs-toggle="modal" data-bs-target="#updateModal" >Modifiez cette publication</button>
      </div>

      {isAdmin() && <>
      <p>Supprimez des photos(admin)</p>
        {post.pictures?.map(item =>
        <>
        <div className="mb-1">
          <img src={prefixThumbnail + item.src} key={item.id} className="w-25" />
          <button className="btn btn-danger ms-3" data-bs-toggle="modal" 
          data-bs-target="#deleteModal" onClick={() => setIdPicture(Number(item.id))}>
            Supprimer</button>
        </div>
        </>
        )}
      </>
      }



      {/* modals */}
      <div className="modal fade" id="addPicture" aria-labelledby="addPictureLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h1 className="modal-title fs-5" id="addPictureLabel">Ajoutez une photo à cette publication</h1>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div className="modal-body">
              {token ?
                <>
                  <form onSubmit={handleSubmit}>
                    <input type="file" name="src" onChange={handleFile} className="form-control" />
                    <div className="d-flex justify-content-center mt-3">
                      <button type="submit" className="btn btn-success text-light rounded-pill">Ajouter</button>
                    </div>

                  </form>
                </> :
                <>
                  <p>Vous devez être connecté pour ajouter une photo.</p>
                  <div className="d-flex justify-content-center ">
                    <button className="btn btn-primary rounded-pill my-3" data-bs-dismiss="modal" aria-label="Close" onClick={modalBtn}>Connexion</button>
                  </div>
                </>
              }
            </div>
            <div className="modal-footer justify-content-center">
              <button type="button" className="btn btn-secondary rounded-pill" data-bs-dismiss="modal">Annuler</button>


            </div>
          </div>
        </div>
      </div>

      <div className="modal fade" id="updateModal" aria-labelledby="updateModalLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h1 className="modal-title fs-5" id="updateModalLabel">Modifiez cette publication</h1>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div className="modal-body">
              {token ?
                <>
                  <form onSubmit={updateSubmit}>
                    <div className="mb-3">
                      <label htmlFor="country" className="form-label">Pays</label>
                      <input type="text" name="country" value={updatedPost.country} onChange={handleUpdateChange} className="form-control" id="country" />
                    </div>
                    <div className="mb-3">
                      <label htmlFor="city" className="form-label">Ville</label>
                      <input type="text" name="city" value={updatedPost.city} onChange={handleUpdateChange} className="form-control" id="city" />
                    </div>
                    <div className="mb-3">
                      <label htmlFor="address" className="form-label">Adresse</label>
                      <input type="text" name="address" value={updatedPost.address} onChange={handleUpdateChange} className="form-control" id="address" />
                    </div>
                    <div className="mb-3">
                      <label htmlFor="description" className="form-label">Description</label>
                      <input type="text" name="description" value={updatedPost.description} onChange={handleUpdateChange} className="form-control" id="description" />
                    </div>
                    <div className="mb-3">
                      <label htmlFor="vegan" className="form-label">Choisissez le type de menu*</label>
                      <select className="form-select" aria-label="vegan" name="fullVegan" value={String(updatedPost.fullVegan)} onChange={handleUpdateChange}>
                        <option value="false">Menu avec options vegans</option>
                        <option value="true">Menu 100% vegan</option>
                      </select>
                    </div>
                    <div className="mb-3">
                      <label htmlFor="website" className="form-label">Site</label>
                      <input type="text" name='website' value={updatedPost.website} onChange={handleUpdateChange} className="form-control" id="website" />
                    </div>
                    <div className="text-center">
                      <button type="submit" className="btn btn-success text-light rounded-pill"  data-bs-dismiss="modal" aria-label="Close">Publier</button>
                    </div>
                  </form>
                </> :
                <>
                  <p>Vous devez être connecté pour modifier cette publication.</p>
                  <div className="d-flex justify-content-center ">
                    <button className="btn btn-primary rounded-pill my-3" data-bs-dismiss="modal" aria-label="Close" onClick={modalBtn}>Connexion</button>
                  </div>
                </>
              }
            </div>
            <div className="modal-footer justify-content-center">
              <button type="button" className="btn btn-secondary rounded-pill" data-bs-dismiss="modal">Annuler</button>


            </div>
          </div>
        </div>
      </div>


      <div className="modal fade" id="deleteModal" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h1 className="modal-title fs-5" id="deleteModalLabel">Supprimez une image</h1>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div className="modal-body">
              <p>Etes-vous sûr de vouloir supprimer cette image ?</p>
              <form onSubmit={deletePic} id="delete-form">
                <div className="d-flex justify-content-center">
                  <button type="submit" className="btn btn-danger text-white rounded-pill my-3 " data-bs-dismiss="modal" aria-label="Close">Supprimer</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

    </>
  )

}

export const getServerSideProps: GetServerSideProps<Props> = async (context) => {
  const { id } = context.query;

  try {
    return {
      props: {
        post: await fetchOnePost(Number(id)),
      }
    }
  } catch {
    return {
      notFound: true
    }
  }

}