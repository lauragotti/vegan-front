import { AuthContext } from "@/auth/auth-context";
import { Picture, Post } from "@/entities";
import { postOnePicture, postOnePost } from "@/post-service";
import Link from "next/link";
import { useRouter } from "next/router";
import { useContext, useEffect, useState } from "react";


export default function Add() {
  const router = useRouter();

  const { token, setToken } = useContext(AuthContext);

  useEffect(() => {
    if (!token) {
      router.push('/login');
    }
  }, [])

  const [errors, setErrors] = useState('')

  const [post, setPost] = useState<Post>({
    name: '',
    country: '',
    city: '',
    address: '',
    description: '',
    fullVegan: false,
    visible: true,
    website: '',
  })

  const [picture, setPicture] = useState<Picture>();

  function handleChange(event: any) {
    if (event.target.name == "fullVegan") {
      setPost({
        ...post,
        [event.target.name]: event.target.value == "true" ? true : false
      });
    } else {
      setPost({
        ...post,
        [event.target.name]: event.target.value
      });
    }
  }

  function handleFile(event: any) {
    const reader = new FileReader();
    reader.onload = () => {
      setPicture({ src: reader.result as string })
    }
    reader.readAsDataURL(event.target.files[0]);
  }

  async function handleSubmit(event: any) {
    event.preventDefault();
    try {
      const add = await postOnePost(post);
      console.log(add)
      await postOnePicture({ ...picture, idPost: add.id });
      router.push('/post/' + add.id);
    } catch (error: any) {
      if (error.response.status == 400) {
        setErrors(error.response.data.detail)
      }
    }
  }

  return (
    <>
      <h1 className="h3 my-3">Ajoutez un lieu</h1>
      <div className="row justify-content-center">
        <div className="col-md-8">

          <form onSubmit={handleSubmit}>
            <div className="mb-3">
              <label htmlFor="name" className="form-label">Nom*</label>
              <input type="text" name='name' value={post.name} onChange={handleChange} className="form-control" id="name" required />
            </div>
            <div className="mb-3">
              <label htmlFor="country" className="form-label">Pays*</label>
              <input type="text" name='country' value={post.country} onChange={handleChange} className="form-control" id="country" required />
            </div>
            <div className="mb-3">
              <label htmlFor="city" className="form-label">Ville*</label>
              <input type="text" name='city' value={post.city} onChange={handleChange} className="form-control" id="city" required />
            </div>
            <div className="mb-3">
              <label htmlFor="address" className="form-label">Adresse*</label>
              <input type="text" name='address' value={post.address} onChange={handleChange} className="form-control" id="address" required />
            </div>
            <div className="mb-3">
              <label htmlFor="description" className="form-label">Description*</label>
              <textarea name='description' value={post.description} onChange={handleChange} className="form-control" id="description" required />
            </div>
            <div className="mb-3">
              <label htmlFor="vegan" className="form-label">Choisissez le type de menu*</label>
              <select className="form-select" aria-label="vegan" name="fullVegan" onChange={handleChange}>
                <option value="false">Menu avec options vegans</option>
                <option value="true">Menu 100% vegan</option>
              </select>
            </div>
            <div className="mb-3">
              <label htmlFor="website" className="form-label">Site</label>
              <input type="text" name='website' value={post.website} onChange={handleChange} className="form-control" id="website" />
            </div>
            <div className="mb-3">
              <label htmlFor="src" className="form-label">Ajoutez une photo*</label>
              <input type="file" name="src" onChange={handleFile} className="form-control" required />
            </div>
            <div className="text-center">
              <button type="submit" className="btn btn-success text-light rounded-pill">Publier</button>
            </div>
          </form>
        </div>
      </div>
    </>
  )
}


