import { AuthContext } from "@/auth/auth-context";
import AddBtn from "@/components/AddBtn";
import PostCard from "@/components/PostCard";
import { Post } from "@/entities";
import { fetchAllPosts } from "@/post-service";
import jwtDecode from "jwt-decode";
import { GetServerSideProps } from "next";
import { useContext } from "react";


interface Props {
  posts: Post[];
}

export default function Home({ posts }: Props) {
  //to sort by popularity
  const visibleSortedPosts = posts
    .filter(item => item.visible) // Filter visible posts
    .sort((a, b) => {
      const likesA = a.likes ? a.likes.length : 0; 
      const likesB = b.likes ? b.likes.length : 0; 
      return likesB - likesA;
    });

  // only show 6 in the front page
  const displayedPosts = visibleSortedPosts.slice(0, 6);


  //admin view
  const { token } = useContext(AuthContext);

  function isAdmin() {
    if (token) {
      const decoded = jwtDecode<any>(token);
      if (decoded.roles[0] == "ROLE_ADMIN") {
        return true;
      }
      return false
    }
  }


  return (
    <>
      <AddBtn />
      <h1 className="h3">Lieux populaires</h1>

      <div className="row">
        {displayedPosts.map((item) => (
          item.visible && 
          <div className="col-lg-6 p-2" key={item.id}>
            <PostCard post={item} key={item.id}/>
          </div>
        ))}
      </div>
      {isAdmin() &&
      <>
      <h2 className="h3 mt-3">Admin: tous les posts</h2>
       <div className="row">
       {posts.map((item) => (
         
         <div className="col-lg-6 p-2" key={item.id}>
           <PostCard post={item} key={item.id}/>
         </div>
       ))}
     </div>
     </>
      }
    </>
  );
}

export const getServerSideProps: GetServerSideProps<Props> = async (context) => {
 try {
   return {
     props: {
       posts: await fetchAllPosts()
      }
    };
  } catch {
    return {
      notFound:true
    }
  }
};
